public with sharing class MerchandiseItemController {

    @AuraEnabled
    public static void addToInvoice(Merchandise__c merchandise){
        String userId = UserInfo.getUserId();
        
        List<Invoice_Item__c> invoices = [
                SELECT Id, Name, Merchandise__c, Quantity__c, Basket__c, Total__c
                FROM Invoice_Item__c
                WHERE UserId__c = :userId
            		AND Basket__r.Status__c = 'Pending'
        ];
        
        if(!invoices.isEmpty()){
            Id merchId;
            
            for(Invoice_Item__c invoice : invoices){
                if(merchandise.Id == invoice.Merchandise__c){
                    merchId = invoice.Merchandise__c;
                    if( merchandise.Quantity__c != invoice.Quantity__c) invoice.Quantity__c ++;
                    
                    update invoice;
                } 
            }
            
            if(merchandise.Id != merchId){
                Invoice_Item__c invoice = new Invoice_Item__c();
                insert invoice;
                invoice.Basket__c = invoices[0].Basket__c;
                invoice.Merchandise__c = merchandise.Id;
                invoice.UserId__c = userId;
            	invoice.Unit_Price__c = merchandise.Price__c;
                invoice.Quantity__c = 1;
                
                update invoice;
                
            } 
            
        } else {
            Invoice_Item__c invoiceItem = new Invoice_Item__c();
            Basket__c basket = new Basket__c(Status__c ='Pending');
            
            insert invoiceItem;
            insert basket;

            invoiceItem.Basket__c = basket.Id;
            invoiceItem.Merchandise__c = merchandise.Id;
            invoiceItem.UserId__c = userId;
            invoiceItem.Unit_Price__c = merchandise.Price__c;
            invoiceItem.Quantity__c = 1;
 			
            update invoiceItem;
            update basket;
       
        } 
    }
}