public with sharing class MerchandiseCategoryListController {

    @AuraEnabled(Cacheable = true)
    public static List<Merchandise_Category__c> getCategories(){
        return [SELECT Name, Id FROM Merchandise_Category__c];
    }
}