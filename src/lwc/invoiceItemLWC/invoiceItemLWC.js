import {LightningElement, api, track} from 'lwc';
import changeQuantity from '@salesforce/apex/BasketController.changeInvoiceQuantity';
import removeFromInvoice from '@salesforce/apex/BasketController.removeFromInvoice';

export default class InvoiceItemLwc extends LightningElement {

    @api invoice;

    invoiceQuantity(event){
        let inputValue = event.target.value;
        let maxMerchValue = event.target.getAttribute('max');

        if(inputValue < 1 || inputValue > maxMerchValue) return;

        changeQuantity({
            invoice : this.invoice,
            counter : parseInt(inputValue)
        }).then(() => {this.dispatchEvent(new CustomEvent('changequantity'));}).catch(error => {});
    }

    remove(){
        removeFromInvoice({
            invoice : this.invoice
        }).then(() => {this.dispatchEvent(new CustomEvent('remove'))}).catch();
    }
}