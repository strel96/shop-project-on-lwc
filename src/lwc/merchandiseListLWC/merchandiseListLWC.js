import {LightningElement, wire, api, track} from 'lwc';
import getMerchandises from '@salesforce/apex/MerchandiseListController.getMerchandise';

export default class MerchandiseListLwc extends LightningElement {

    @track merchandises;

    connectedCallback() {
        this.href = window.location.href.split('/');
        this.id = this.href[ this.href.length - 2 ];

        getMerchandises({id : this.id}).then(res => {this.merchandises = res;});
    }

    handleAction(){
        this.dispatchEvent(new CustomEvent('merch'));
    }

    @api
    rerenderMerchandise(){
        getMerchandises({id : this.id}).then(res => {
            this.merchandises = undefined;
            this.merchandises = res;
        });
    }
}