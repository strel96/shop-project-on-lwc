import {LightningElement, api, wire} from 'lwc';
import addToInvoice from '@salesforce/apex/MerchandiseItemController.addToInvoice';

export default class MerchandiseItemLwc extends LightningElement {

    @api merchandise;

    addItemToBasket(){
        addToInvoice({merchandise : this.merchandise})
            .then(() =>{
                this.dispatchEvent(new CustomEvent('addtobasket'));
            })
            .catch(error =>{});
    }
}