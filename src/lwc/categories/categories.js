import {LightningElement, wire} from 'lwc';
import {NavigationMixin} from 'lightning/navigation';
import getCategories from '@salesforce/apex/MerchandiseCategoryListController.getCategories';

export default class Categories extends NavigationMixin(LightningElement) {

    @wire(getCategories)
    categories;

    navigateToRecord(event) {
        this.merchandiseDetailPage = {
            type: 'standard__recordPage',
            attributes: {
                recordId: event.target.getAttribute('data-category'),
                objectApiName: 'Merchandise_Category__c',
                actionName: 'view'
            }
        };

        event.preventDefault();
        event.stopPropagation();

        this[NavigationMixin.Navigate](this.merchandiseDetailPage);
    }
}