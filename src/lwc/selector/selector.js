import {LightningElement} from 'lwc';

export default class Selector extends LightningElement {

    handleBuy(){
        this.template.querySelector('c-merchandise-list-l-w-c').rerenderMerchandise();
    }

    handleMerch(){
        this.template.querySelector('c-invoice-list-l-w-c').rerenderInvoice();
    }
}