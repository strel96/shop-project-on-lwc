import {LightningElement, wire, api, track} from 'lwc';
import { refreshApex } from '@salesforce/apex';
import getInvoices from '@salesforce/apex/BasketController.getInvoiceItems';
import buyItems from'@salesforce/apex/BasketController.buyInvoiceItems';
import deleteAllItems from '@salesforce/apex/BasketController.deleteAllInvoice';

export default class InvoiceListLwc extends LightningElement {

    @track
    invoices;

    @api
    rerenderInvoice() {
        getInvoices().then(res => {this.invoices = undefined; this.invoices = res;}).catch(error => {});
    }

    connectedCallback() {
        getInvoices().then(res => {this.invoices = res;}).catch(error => {});
        console.log(this.invoices);
    }

    handleButtonBuy(){
        buyItems({invoices : this.invoices}).then(() => {
            this.invoices = undefined;
            this.dispatchEvent(new CustomEvent('buy'));
        }).catch(error => {});
    }

    handleButtonDeleteAll(){
        deleteAllItems({invoices : this.invoices}).then(() => {this.invoices = undefined;}).catch(error => {});
    }
}