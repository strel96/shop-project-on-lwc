trigger MerchandiseTrigger on Merchandise__c (before update) {
    MerchandiseTriggerHandler merchandiseTriggerHandler = new MerchandiseTriggerHandler();

    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            merchandiseTriggerHandler.addNewItem(Trigger.new);
        }
    }
}